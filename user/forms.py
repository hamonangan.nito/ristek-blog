from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

# Create your forms here.
class NewUserForm(UserCreationForm):

    first_name = forms.CharField(required=True, max_length=30)
    last_name = forms.CharField(required=True, max_length=40)
    username = forms.CharField(required=True, max_length=25)
    password1 = forms.CharField(widget=forms.PasswordInput, required=True, max_length=25)
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}), required=True, max_length=25)
    
    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "password1", "password2")
