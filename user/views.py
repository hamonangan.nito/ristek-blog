from django.shortcuts import render, redirect
from .forms import NewUserForm

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages

def register_request(request):
    if request.method == "POST":
        form = NewUserForm(request.POST or None)
        if form.is_valid():
            user = form.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            messages.success(request, "Registration success!" )
            return redirect("/")
        messages.error(request, "Oh no, your registration failed! Please try again with correct information...")
    
    form = NewUserForm()
    response =  { "form":form }
    return render (request, "register.html", response)

def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None: # user already existed
                login(request, user)
                messages.info(request, f"Logged in as {username}.")
                return redirect("/")
            
            messages.error(request, "Login failed :( Please register if you don't have an account...")
        
        else: # Invalid form
            messages.error(request, "Login failed :( Try again with correct username and password")
    
    # GET request
    form = AuthenticationForm()
    response = {"form" : form}
    return render(request, "login.html", response)

def logout_request(request):
    logout(request)
    messages.info(request, "Successfully logged out. See ya!") 
    return redirect("/")
