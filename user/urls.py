from django.urls import path
from .views import register_request, login_request, logout_request

urlpatterns = [
    path('register', register_request, name='register_request'),
    path('login', login_request, name='login_request'),
    path('logout', logout_request, name='logout_request')
]
