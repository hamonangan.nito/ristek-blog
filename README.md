# Ristek-blog project

Dev:
- Bornyto Hamonangan (2006486084)

### Situs web

Proyek ini sudah di-deploy ke https://nito-dev.herokuapp.com. Apabila tertarik untuk mendevelop proyek ini, Anda bisa menjalankannya secara lokal di komputer Anda.

Catatan: Proyek ini menggunakan Bootstrap CDN sehingga Anda perlu koneksi internet untuk menampilkan style dengan utuh.

### Panduan instalasi Python

Proyek ini dibuat dengan framework Django menggunakan bahasa Python. Untuk dapat menjalankan program Python, unduh terlebih dahulu [Python](https://www.python.org/downloads/).

Cek versi Python dan pastikan sudah terinstal dengan mengetik 'python --version' pada terminal/CMD.

Jika belum ada coba cek PATH variabel, bisa ikuti [tutorial ini](https://www.tutorialspoint.com/python/python_environment.htm).

Apabila Python sudah terinstal maka proyek ini sudah bisa dikembangkan.

### Panduan menjalankan proyek

Untuk menjalankan program pada komputer lokal, buat terlebih dahulu virtual environment. Langkah-langkahnya adalah sbb:

1. Tempatkan direktori terminal/CMD pada folder/direktori projek ini (bisa juga di luar, untuk memudahkan tutorial bisa ikuti langkah ini).

2. Ketikkan 'python -m venv venv' pada terminal. Akan terbentuk virtual environment bernama venv.

3. Aktifkan virtual environment dengan mengetik 'venv\Scripts\activate'

Note: Powershell mungkin tidak bisa digunakan untuk ini, jadi pilih CMD untuk windows.

Setelah itu, unduh berbagai dependensi yang dibutuhkan. Pastikan virtual environment aktif lalu ketikkan 'pip install -r requirements.txt' pada terminal. Ini akan mengunduh seluruh berkas yang diperlukan.

Sekarang proyek sudah siap untuk di-develop. Untuk menjalankan server, ketikkan 'python manage.py runserver' pada terminal dengan terlebih dahulu mengaktifkan virtual environment.

Untuk membuat akun admin dan mengakses Django Admin (ini akan diperlukan untuk membuat blog dan moderasi) ketikkan 'python manage.py createsuperuser' pada terminal. Isi kredensial yang diperlukan (username perlu diisi) dan akun Anda dapat dipakai untuk mengakses Django Admin dengan langkah sbb:

1. Jalankan dulu servernya ('python manage.py runserver')

2. Masuk ke Django Admin (http://127.0.0.1:8000/admin)

3. Login admin dengan username dan password yang telah dibuat

4. Akses link berikut: [Moderasi dan membuat blog](http://127.0.0.1:8000/admin/blog/blog/), [Moderasi komentar](http://127.0.0.1:8000/admin/blog/blogcomment/), [Moderasi user](http://127.0.0.1:8000/admin/auth/user/)

Proyek sudah siap untuk didevelop!

### Panduan autentikasi

Proyek ini mendukung autentikasi manual (isi username, password) dan otomatis dengan akun Google. Login via Google bisa diakses dari [sini](http://127.0.0.1:8000/accounts/google/login/) sedangkan manual bisa diakses dari [sini](http://127.0.0.1:8000/user/login) Ada beberapa hal yang perlu diperhatikan agar autentikasi dengan Google bisa dilakukan di komputer lokal.

1. Akses web via http://127.0.0.1:8000/. Alternatif seperti http://localhost:8000/ tidak dapat digunakan untuk autentikasi akun Google.

2. Buka [Google Developers Console](https://console.developers.google.com/) lalu klik Credentials pada side navbar. Jika belum pernah, buat dulu proyek baru dan isi consent form. Klik CREATE PROJECT dan isi nama web (apa saja yang berhubungan), kemudian klik Create. Dalam Consent Form, pilih External (supaya bisa diakses email lain). Masukkan nama app dan email (Anda bisa mengisi yang diperlukan saja).

Jika sudah, kembali ke Credentials, klik Create Credentials, dan pilih OAuth Client ID.

3. Pilih Web Application, masukkan nama, dan masukkan URL. Untuk origin URI, masukkan http://127.0.0.1:8000/ dan untuk redirect URI masukkan http://127.0.0.1:8000/accounts/google/login/callback/ 

4. Klik Create dan akan muncul Client ID dan Client Secret.

5. Jalankan server dengan 'python manage.py runserver' kemudian buka pengaturan untuk Google [di sini](http://localhost:8000/admin/socialaccount/socialapp/1/change/). Login dengan akun admin apabila belum login, lalu coba akses link di atas lagi. Pilih 'Google' sebagai provider, ketik 'Google' sebagai name, lalu copy-paste client ID dan secret key yang didapatkan dari Google Developers Console. Kosongkan key. Terakhir, tambahkan example.com sebagai sites.

Simpan pengaturan dan Anda dapat mencoba autentikasi dengan Google. User yang terautentikasi dapat mengakses fitur komentar pada blog, yaitu membuat, mengedit dan menghapus komentarnya sendiri.


