from django.shortcuts import render

def home(request):
    if (request.user.is_authenticated):
        return render(request, 'main/home.html', {'user': request.user})
    return render(request, 'main/home.html')

