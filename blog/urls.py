from django.urls import path
from .views import get_all_blogs, get_blog_by_url, post_comment, put_comment, delete_comment

urlpatterns = [
    path('', get_all_blogs, name='get_all_blogs'),
    path('<blog_url>', get_blog_by_url, name='get_blog_by_url'),
    path('<blog_url>/post-comment', post_comment, name='post_comment'),
    path('<blog_url>/put-comment/<comment_id>', put_comment, name='put_comment'),
    path('<blog_url>/delete-comment/<comment_id>', delete_comment, name='delete_comment'),
]
