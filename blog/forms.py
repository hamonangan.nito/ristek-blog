from django.forms import ModelForm

from blog.models import Blog, BlogComment

class BlogForm(ModelForm):
    class Meta:
        model = Blog
        fields = ['url', 'title', 'content']

class BlogCommentForm(ModelForm):
    class Meta:
        model = BlogComment
        fields = ['content']