from django.db import models

from datetime import date, datetime
from django.conf import settings
import random

class Blog(models.Model):
    def save(self, *args, **kwargs):
        if not self.url: # first POST only
            self.created_at = datetime.now()

        self.updated_at = datetime.now()
        return super(Blog, self).save(*args, **kwargs)

    url = models.CharField(primary_key=True, max_length=100)
    title = models.CharField(max_length=100)
    content = models.TextField()
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now)

    class Meta:
        ordering = ['-updated_at'] # sort by recent

    

    def __str__(self):
        return self.title

class BlogComment(models.Model):
    def generateId():
        currentId = ""
        for i in range(5):
            randomChar = chr(random.randint(65, 90))
            currentId += randomChar
        return currentId
    
    def save(self, *args, **kwargs):
        if not self.id: # first POST only
            self.created_at = datetime.now()

        self.updated_at = datetime.now()
        return super(BlogComment, self).save(*args, **kwargs)

    id = models.CharField(primary_key=True, default=generateId, max_length=5, editable=False)
    content = models.TextField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(default=datetime.now)

    class Meta:
        ordering = ['created_at'] # sort by first created

    

    

    def __str__(self):
        return self.id