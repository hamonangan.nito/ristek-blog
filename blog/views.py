from xml.etree.ElementTree import Comment
from django.http import Http404
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied

from blog.forms import BlogCommentForm

from .models import Blog, BlogComment

def get_all_blogs(request):
    blogs = Blog.objects.all().values()
    if (request.user.is_authenticated):
        response = {
            'blogs' : blogs, 
            'user': request.user # logged in
        }  
    else: 
        response = {
            'blogs' : blogs,
        }  
    return render(request, 'blog.html', response)

def get_blog_by_url(request, blog_url):
    blog = Blog.objects.get(pk=blog_url)
    comments = BlogComment.objects.filter(blog=blog_url)
    response = {}
    if (request.user.is_authenticated):
        response = {
            'blog' : blog, 
            'comments': comments,
            'user': request.user # can add or edit owned comment
        }  
    else: 
        response = {
            'blog' : blog, 
            'comments': comments
        }  
    return render(request, 'blog_detail.html', response)

@login_required(login_url='/user/login')
def post_comment(request, blog_url):
    form = BlogCommentForm(request.POST or None)
    if form.is_valid():
        new_comment = form.save(commit=False)
        new_comment.author = request.user
        new_comment.blog = Blog.objects.get(pk=blog_url)
        new_comment.save()
        form.save_m2m()
        return redirect('/blog/' + blog_url)
    
    # GET Request
    response = {'form':form}
    return render(request, 'comment_form.html', response)

@login_required(login_url='/user/login')
def put_comment(request, blog_url, comment_id):
    try:
        comment = BlogComment.objects.get(pk=comment_id)
        if (comment.author != request.user):
            messages.success(request, 'Access denied...')
            raise PermissionDenied()
    except BlogComment.DoesNotExist:
        raise Http404("Not found")

    form = BlogCommentForm(request.POST or None, instance=comment)
    if form.is_valid():
        form.save()
        return redirect('/blog/' + blog_url)
    
    # GET Request
    response = {'form':form}
    return render(request, 'comment_form.html', response)

@login_required(login_url='/user/login')
def delete_comment(request, blog_url, comment_id):
    try:
        comment = BlogComment.objects.get(pk=comment_id)
        if (comment.author != request.user):
            messages.success(request, 'Access denied...')
            raise PermissionDenied()
        comment.delete()
        messages.success(request, 'Your comment is successfully deleted :)')
        return redirect('/blog/' + blog_url)
    except BlogComment.DoesNotExist:
        raise Http404("Not found")
        
